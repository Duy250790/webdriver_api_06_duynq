package Exersice;

import org.testng.annotations.Test;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class XpathAndCSS {
	WebDriver driver;
	private String messageRequired = "This is a required field.";
	private String messageValidEmail = "Please enter a valid email address. For example johndoe@domain.com.";
	private String messageValidPassword = "Please enter 6 or more characters without leading or trailing spaces.";
	private String messageIncorectPassword = "Invalid login or password.";
	private String validEmail = "12121212@121212.121212";
	private String email = "automation_13@gmail.com";
	private String passwordless6character = "123";
	private String passIncorrect = "123123123";
	private String passwordCorrect = "123123";
	
	
	//@Test
	public void TC01_VerifywithemptyEmailandPassword() {
		driver.get("http://live.guru99.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();	
		
		driver.findElement(By.xpath("//button[@id='send2']")).click();	
		
		Assert.assertEquals(driver.findElement(By.id("advice-required-entry-email")).getText(), messageRequired);
		Assert.assertEquals(driver.findElement(By.id("advice-required-entry-pass")).getText(), messageRequired);
	}
	
	//@Test
	public void TC02_LoginwithinvalidEmail() {
		driver.get("http://live.guru99.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();		
		
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();	
		
		driver.findElement(By.name("login[username]")).sendKeys(validEmail);		
		driver.findElement(By.id("send2")).click();	
		
		Assert.assertEquals(driver.findElement(By.id("advice-validate-email-email")).getText(), messageValidEmail);
	}
	
	//@Test
	public void TC03_LoginwithPasswordless6character() {
		driver.get("http://live.guru99.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();		
		
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();	
		
		driver.findElement(By.name("login[username]")).sendKeys(email);
		driver.findElement(By.name("login[password]")).sendKeys(passwordless6character);		
		driver.findElement(By.id("send2")).click();	
		
		Assert.assertEquals(driver.findElement(By.id("advice-validate-password-pass")).getText(), messageValidPassword);
	}
	
	//@Test
	public void TC04_LoginwithIncorrectPassword() {
		driver.get("http://live.guru99.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();		
		
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();	
		
		driver.findElement(By.name("login[username]")).sendKeys(email);
		driver.findElement(By.name("login[password]")).sendKeys(passIncorrect);		
		driver.findElement(By.id("send2")).click();	
		
		Assert.assertEquals(driver.findElement(By.xpath("//li[@class='error-msg']/ul/li/span")).getText(), messageIncorectPassword);
	}
	
	//@Test
	public void TC05_LoginwithvalidEmailandPassword() {
		driver.get("http://live.guru99.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();		
		
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();	
		
		driver.findElement(By.name("login[username]")).sendKeys(email);
		driver.findElement(By.name("login[password]")).sendKeys(passwordCorrect);		
		driver.findElement(By.id("send2")).click();	
		
		Assert.assertEquals(driver.findElement(By.xpath("//div[@class='page-title']/h1")).getText(), "MY DASHBOARD");		
		Assert.assertEquals(driver.findElement(By.xpath("//p[@class='hello']/strong")).getText(), "Hello, Automation Testing!");		
		Assert.assertEquals(driver.findElement(By.xpath("//div[@class='box-content']/p[contains(text(),'Automation Testing')]")).getText(),"Automation Testing\n"+email+"\nChange Password");
		System.out.println("Automation Testing\n"+email+"\nChange Password");
	}
	
	@Test
	public void TC06_CreateANewAccount() throws InterruptedException {		
		String firstName="Test";
		String lastName= "Exercise";
		String passWord ="12341234";
		String emailAddress= "AutoExercise"+RandomNumber()+"@gmail.com";
		
		driver.get("http://live.guru99.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();		
		
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
		
		driver.findElement(By.xpath("//a[@title='Create an Account']")).click();
		driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys(firstName);
		driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys(lastName);
		driver.findElement(By.xpath("//input[@id='email_address']")).sendKeys(emailAddress);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(passWord);
		driver.findElement(By.xpath("//input[@id='confirmation']")).sendKeys(passWord);	
		driver.findElement(By.xpath("//button[@title='Register']")).click();
		
		Assert.assertEquals(driver.findElement(By.xpath("//li[@class='success-msg']")).getText(), "Thank you for registering with Main Website Store.");
		
		driver.findElement(By.xpath("//a[@class='skip-link skip-account']/span[text()='Account']")).click();
		
		driver.findElement(By.xpath("//a[@title='Log Out']")).click();
		Thread.sleep(8000);
		Assert.assertEquals(driver.getCurrentUrl(), "http://live.demoguru99.com/index.php/");
	}
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
	}
	
	@AfterClass
	public void afterClass() {
		//Clear data
		driver.quit();
	}
	
	public int RandomNumber() {
		Random ran =new Random(); 
		int result_random = ran.nextInt(9999);
		return result_random;
	}
}
