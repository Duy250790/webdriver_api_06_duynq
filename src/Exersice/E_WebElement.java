package Exersice;

import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class E_WebElement {
	WebDriver driver;
	
	//@Test
	public void TC01_CheckElementisDisplay() throws InterruptedException {
		driver.get("https://automationfc.github.io/basic-form/index.html");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		WebElement emailTexbox = driver.findElement(By.id("mail"));
		WebElement ageUnder18 = driver.findElement(By.id("under_18"));
		WebElement educationAreabox = driver.findElement(By.id("edu"));
		
		if(emailTexbox.isDisplayed()&&ageUnder18.isDisplayed() &&educationAreabox.isDisplayed()  ) {
			emailTexbox.sendKeys("Autonmation Testing");
			educationAreabox.sendKeys("Autonmation Testing");
			ageUnder18.click();
		}
		else return;
		
		Thread.sleep(5000);
	}
	
	@Test
	public void TC02_CheckElementisEnableDisable() throws InterruptedException {
		driver.get("https://automationfc.github.io/basic-form/index.html");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		WebElement emailTexbox = driver.findElement(By.id("mail"));
		WebElement ageUnder18 = driver.findElement(By.id("under_18"));
		WebElement educationAreabox = driver.findElement(By.id("edu"));
		WebElement Job1Dropdown = driver.findElement(By.id("job1"));
		WebElement developmentCheckbox = driver.findElement(By.id("development"));
		WebElement slider = driver.findElement(By.id("slider-1"));
		
		if(emailTexbox.isEnabled()) {
			System.out.println("emailTexbox is Enable");
		} else System.out.println("emailTexbox is Disable");
		
		if(ageUnder18.isEnabled()) {
			System.out.println("ageUnder18 is Enable");
		} else System.out.println("ageUnder18 is Disable");
		
		if(educationAreabox.isEnabled()) {
			System.out.println("educationAreabox is Enable");
		} else System.out.println("educationAreabox is Disable");
		
		if(Job1Dropdown.isEnabled()) {
			System.out.println("Job1Dropdown is Enable");
		} else System.out.println("Job1Dropdown is Disable");
		
		if(developmentCheckbox.isEnabled()) {
			System.out.println("developmentCheckbox is Enable");
		} else System.out.println("developmentCheckbox is Disable");
		
		if(slider.isEnabled()) {
			System.out.println("slider is Enable");
		} else System.out.println("slider is Disable");
		
		Thread.sleep(5000);
	}
	

	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver", ".\\lib\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@AfterClass
	public void afterClass() {
		//Clear data
		driver.quit();
	}
}
