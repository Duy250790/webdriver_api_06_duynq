package WebDriver_TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNG_01_Annotation {

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("beforeMethod");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("afterMethod");
	}

	@Test
	public void TC_01_VerifyURLandtitle() {
		System.out.println("TC_01_VerifyURLandtitle");
	}
	@Test
	public void TC_02_Loginempty() {
		System.out.println("TC_02_Loginempty");
	}	
	
	@Test
	public void TC_03_LoginwithEmailinvalid() {
		System.out.println("TC_03_LoginwithEmailinvalid");		
	}	

	@BeforeClass
	public void beforeClass() {
		System.out.println("beforeClass");
	}

	@BeforeTest
	public void beforeTest() {
		System.out.println("beforeTest");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("afterTest");
	}

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("beforeSuite");
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("afterSuite");
	}

}
