package WebDriver_TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNG_03_Priority_Skip {

	@Test(groups="Shopping", priority = 6)
	public void TC_01_VerifyURLandtitle() {
		System.out.println("TC_01_VerifyURLandtitle");
	}
	@Test(groups="Shopping", priority = 5)
	public void TC_02_Loginempty() {
		System.out.println("TC_02_Loginempty");
	}	
	
	@Test(groups="Shopping", priority = 4,enabled =false)
	public void TC_03_LoginwithEmailinvalid() {
		System.out.println("TC_03_LoginwithEmailinvalid");		
	}
	
	@Test(groups="Buying", priority = 3)
	public void TC_04_Order() {
		System.out.println("TC_04_Order");
	}
	@Test(groups="Buying", priority = 2,enabled =false)
	public void TC_05_Payment() {
		System.out.println("TC_05_Payment");
	}	
	
	@Test(groups="Buying", priority = 1, enabled =true)
	public void TC_06_Invoice() {
		System.out.println("TC_06_Invoice");		
	}		

}
