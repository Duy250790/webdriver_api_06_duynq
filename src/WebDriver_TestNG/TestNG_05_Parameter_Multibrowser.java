package WebDriver_TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.BeforeClass;

import java.sql.Driver;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNG_05_Parameter_Multibrowser {
	WebDriver driver;
	String email;
	
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String browserName) {
		//driver = new FirefoxDriver();
		
		if (browserName.equals("firefox")) {
			driver = new FirefoxDriver();
		}
		else if (browserName.equals("chrome")) {
				System.setProperty("webdriver.chrome.driver", ".\\lib\\chromedriver.exe");
				driver = new ChromeDriver();
		}else {
			System.setProperty("webdriver.chrome.driver", ".\\lib\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("headless");
			options.addArguments("window-size=1920x1080");
			driver = new ChromeDriver(options);
		}
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	//@Test
	public void TC_01_RegisterToSystem() throws InterruptedException {
		// Function for testcase	
		email = "selenium06"+ randomdata()+"@gmail.com";		
		driver.get("http://live.guru99.com");
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();		
		driver.findElement(By.xpath("//span[text()='Create an Account']")).click();
		driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("A");
		driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("B");
		driver.findElement(By.xpath("//input[@id='email_address']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//input[@id='confirmation']")).sendKeys("123456");
		driver.findElement(By.xpath("//span[text()='Register']")).click();
		String verifyMessage=driver.findElement(By.xpath("//span[text()='Thank you for registering with Main Website Store.']")).getText();
		Assert.assertEquals(verifyMessage, "Thank you for registering with Main Website Store.");
		driver.findElement(By.xpath("//div[@class='page-header-container']//span[text()='Account']")).click();
		driver.findElement(By.xpath("//a[text()='Log Out']")).click();
		Thread.sleep(10000) ;
		String homepageTitle = driver.getTitle();		
		Assert.assertEquals(homepageTitle, "Home page");	
	}		
	
	@Parameters({"email", "pass"})
	@Test
	public void TC_02_LoginToSystem(String email, String Pass)  {
		// Function for testcase				
		driver.get("http://live.guru99.com");
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys(Pass);
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//h1[text()='My Dashboard']")).isDisplayed());	
	}		

	@AfterClass
	public void afterClass() {
	}
	
	public int randomdata() {
		Random random=new Random();
		int number = random.nextInt(999999);
		return number;
	}	

}
