package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_09_Popup_Window {
	WebDriver driver;
	String titleAgri ="HDFC Bank Kisan Dhan Vikas e-Kendra";	
	String titleAccountDetails ="Welcome to HDFC Bank NetBanking";
	String titlePrivate ="HDFC Bank - Leading Bank in India, Banking Services, Private Banking, Personal Loan, Car Loan";
	
	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	//@Test
	public void TC_01_TestScript02() {
		// Function for testcase
		driver.get("https://daominhdam.github.io/basic-form/index.html");
		String parentWindow = driver.getWindowHandle();
		driver.findElement(By.xpath("//a[text()='Click Here']")).click();
		switchToChildWindow(parentWindow);
		Assert.assertEquals(driver.getTitle(), "Google");
		Assert.assertTrue(closeAllWithoutParentWindows(parentWindow));
	}

	@Test
	public void TC_02_TestScript03() {
		// Function for testcase
		driver.get("http://www.hdfcbank.com/");
		String parentWindow = driver.getWindowHandle();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> notificationIframe = driver
				.findElements(By.xpath("//iframe[@id='vizury-notification-template']"));

		if (notificationIframe.size() > 0) {
			driver.switchTo().frame(notificationIframe.get(0));
			WebElement closePopup = driver.findElement(By.xpath("//*[@id='div-close']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", closePopup);
			driver.switchTo().defaultContent();
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//a[text()='Agri']")).click();
		switchToWindowByTitle(titleAgri);
		
		driver.findElement(By.xpath("//p[text()='Account Details']")).click();
		switchToWindowByTitle(titleAccountDetails);
		
		driver.switchTo().frame(driver.findElement(By.xpath("//frame[@name='footer']")));
		driver.findElement(By.xpath("//a[text()='Privacy Policy']")).click();		
		switchToWindowByTitle(titlePrivate);
		driver.findElement(By.xpath("//a[text()='CSR']")).click();
		
		Assert.assertTrue(closeAllWithoutParentWindows(parentWindow));
	}

	@AfterClass
	public void afterClass() {
		// Clear data
		driver.quit();
	}

	public void switchToChildWindow(String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(parent)) {
				driver.switchTo().window(runWindow);
				break;
			}
		}
	}

	public void switchToWindowByTitle(String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			driver.switchTo().window(runWindows);
			String currentWin = driver.getTitle();
			if (currentWin.equals(title)) {
				break;
			}
		}
	}
	public boolean closeAllWithoutParentWindows(String parentWindow) {
        Set<String> allWindows = driver.getWindowHandles();
        for (String runWindows : allWindows) {
                    if (!runWindows.equals(parentWindow)) {
                                driver.switchTo().window(runWindows);
                                driver.close();
                    }
        }
        driver.switchTo().window(parentWindow);
        if (driver.getWindowHandles().size() == 1)
                   return true;
        else
                   return false;
	}

}
