package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_07_UserInterAction {
	WebDriver driver;
	Actions action;
	List <String> number;
	
	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition		
		System.setProperty("webdriver.chrome.driver", ".\\lib\\chromedriver.exe");
		driver = new ChromeDriver();
		action = new Actions(driver) ;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();		
	}
	
	
	//@Test
	public void TC_01_moveMouseToelement() throws Exception {
		// Function for testcase
		driver.get("http://www.myntra.com/");
		Thread.sleep(2000);
		WebElement accountIcon = driver.findElement(By.xpath("//div[@class='desktop-userIconsContainer']"));
		action.moveToElement(accountIcon).perform();		
		driver.findElement(By.xpath("//a[text()='login']")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//p[text()='Login to Myntra']")).isDisplayed());
	}	

	//@Test
	public void TC_02_clickAndHold() throws Exception {
		// Function for testcase
		number = new ArrayList<>();
		number.add("1");
		number.add("2");
		number.add("3");
		number.add("4");
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");	
		List <WebElement> numberElements = driver.findElements(By.xpath("//ol[@id='selectable']//li"));
		action.clickAndHold(numberElements.get(0)).clickAndHold(numberElements.get(3)).release().perform();
		Thread.sleep(5000);
		
		List<String> numberSelected = getListElement("//li[contains(@class,'ui-selected')]");		
		Assert.assertEquals(number,numberSelected);
		
	}	
	
	//@Test
	public void TC_03_clickAndHoldRandom() throws Exception {
		// Function for testcase		
		number = new ArrayList<>();
		number.add("1");
		number.add("3");
		number.add("5");
		number.add("6");
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");	
		List <WebElement> numberElements = driver.findElements(By.xpath("//ol[@id='selectable']//li"));
		action.keyDown(Keys.LEFT_CONTROL).click(numberElements.get(0)).click(numberElements.get(2)).click(numberElements.get(4))
		.click(numberElements.get(5)).keyUp(Keys.LEFT_CONTROL).release().perform();
		Thread.sleep(5000);
		
		List<String> numberSelected = getListElement("//li[contains(@class,'ui-selected')]");	
		System.out.println(numberSelected);
		Assert.assertEquals(number,numberSelected);
		
	}	
	//@Test
	public void TC_04_doubleClick() throws Exception {
		// Function for testcase
		driver.get("http://www.seleniumlearn.com/double-click");
		Thread.sleep(2000);				
		WebElement buttonDoubleClick= driver.findElement(By.xpath("//button[text()='Double-Click Me!']"));
		action.doubleClick(buttonDoubleClick).perform();
		Thread.sleep(5000);
		Alert alert = driver.switchTo().alert();
		Assert.assertEquals("The Button was double-clicked.", alert.getText());
		alert.accept();
	}	
	
	//@Test
	public void TC_05_rightClick() throws Exception {
		// Function for testcase
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		//Thread.sleep(2000);				
		WebElement buttonRightClick= driver.findElement(By.xpath("//span[text()='right click me']"));
		action.contextClick(buttonRightClick).release().perform();
		
		WebElement buttonQuit=driver.findElement(By.xpath("//li[contains(@class,'context-menu-icon-quit') and not (contains(@class,'context-menu-hover'))]"));
		Assert.assertTrue(buttonQuit.isDisplayed());
		Thread.sleep(2000);
		action.moveToElement(buttonQuit).perform();
		Thread.sleep(2000);
		WebElement buttonQuitVisible=driver.findElement(By.xpath("//li[contains(@class,'context-menu-icon-quit') and contains(@class,'context-menu-hover')]"));
		Assert.assertTrue(buttonQuitVisible.isDisplayed());
		action.click(buttonQuit).perform();
		Thread.sleep(2000);
		Alert alert = driver.switchTo().alert();
		Assert.assertEquals(alert.getText(), "clicked: quit");
		alert.accept();		
	}	
	
	@Test
	public void TC_06_dragAndDropElement01() throws Exception {
		// Function for testcase
		driver.get("http://demos.telerik.com/kendo-ui/dragdrop/angular");
		//Thread.sleep(2000);				
		WebElement buttonCycleSmall =driver.findElement(By.xpath("//div[@id='draggable']"));
		WebElement buttonCycleBig = driver.findElement(By.xpath("//div[@id='droptarget']"));
		action.dragAndDrop(buttonCycleSmall, buttonCycleBig).perform();		
		//action.dragAndDrop(buttonCycleSmall, buttonCycleBig).clickAndHold();
		Thread.sleep(5000);		
		//System.out.println(buttonCycleBig.getText());
		
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='droptarget' and text()='You did great!']")).isDisplayed());
		//Assert.assertTrue(driver.findElement(By.xpath("//div[@id='droptarget' and contains( text()='Now drop...')]")).isDisplayed());
	}	
	
	@Test
	public void TC_07_dragAndDropElement02() throws Exception {
		// Function for testcase
		driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
		//Thread.sleep(2000);				
		WebElement dragSource =driver.findElement(By.xpath("//div[@id='draggable']"));
		WebElement dropTarget = driver.findElement(By.xpath("//div[@id='droppable']"));
		action.dragAndDrop(dragSource, dropTarget).perform();		
		Thread.sleep(5000);		
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='droppable']//p[text()='Dropped!' and not ( text() = 'Drop here')]")).isDisplayed());
	}	
	
	@AfterClass
	public void afterClass() {
		//Clear data
		driver.quit();
	}
	
	
	
	public List <String> getListElement(String element) {
		List<String> listResult=new ArrayList<>();
		List<WebElement> listElement = driver.findElements(By.xpath(element));
		for (WebElement e: listElement)
		{
			listResult.add(e.getText());
		}
		return listResult;
	}

}
