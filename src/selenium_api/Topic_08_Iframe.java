package selenium_api;

import org.testng.annotations.Test;

import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory.Default;

import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.server.browserlaunchers.DrivenSeleniumLauncher;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_08_Iframe {
	WebDriver driver;
	
	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		driver = new FirefoxDriver();	
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	
	@Test
	public void TC_01_Iframe() throws Exception {
		// Function for testcase
		driver.get("http://www.hdfcbank.com/");
		System.out.println("Step 01");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> notificationIframe = driver.findElements(By.xpath("//iframe[@id='vizury-notification-template']"));
		
		if(notificationIframe.size()> 0)
		{
			System.out.println("Step 02");	
			driver.switchTo().frame(notificationIframe.get(0));			
			System.out.println("Click close ICon");		
			WebElement closePopup = driver.findElement(By.xpath("//*[@id='div-close']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", closePopup);
			driver.switchTo().defaultContent();
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		System.out.println("Step 03");	
		WebElement lookingIframe= driver.findElement(By.xpath("//div[@class='flipBannerWrap']//iframe"));
		driver.switchTo().frame(lookingIframe);
		Assert.assertTrue(driver.findElement(By.xpath("//span[@id='messageText' and text()='What are you looking for?']")).isDisplayed());
		
		driver.switchTo().defaultContent();
		
		System.out.println("Step 04");			
		WebElement panerIframe = driver.findElement(By.xpath("//div[@class='slidingbanners']//iframe"));
		driver.switchTo().frame(panerIframe);		
		Assert.assertTrue(driver.findElement(By.xpath("//div[@Class='products-list']")).isDisplayed());
		List<WebElement> listImageBaner= driver.findElements(By.xpath("//div[@id ='bannercontainer']//img"));
		Assert.assertEquals(listImageBaner.size(), 6);
		
		
		System.out.println("Step 05");	
		
		driver.switchTo().defaultContent();
		List<WebElement> fliperIframe = driver.findElements(By.xpath("//div[@class='flipBanner']//div[contains(@class,'product')]"));
		Assert.assertEquals(fliperIframe.size(), 8);			
		
	}	
	

	@AfterClass
	public void afterClass() {
		//Clear data
		//driver.quit();
	}

}
