package selenium_api;

import org.testng.annotations.Test;

import com.sun.org.apache.xerces.internal.impl.xpath.XPath;

import org.testng.annotations.BeforeClass;

import static org.junit.Assert.assertArrayEquals;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_04_Handel_Textbox_TextArea {
	WebDriver driver;
	WebElement element;	
	int random;
	String customerID,customerName, birthOfDay,address,city,state,pin,mobie, email,password;
	String editaddress,editcity,editstate,editpin,editmobie, editemail;
	
	
	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		driver = new FirefoxDriver();	
		customerName="Quang Duy";
		birthOfDay ="1900-03-03";
		address="123 Adress";
		city ="TPHCM";
		state="LA";
		pin="650000";
		mobie="09999999";
		email = "selenium06"+ randomdata()+"@gmail.com";
		password="123456";	
		
		editaddress ="100 Nguyen Minh Khai";
		editcity ="Ha Noi";
		editstate ="Long Bien";
		editpin ="700000";
		editmobie ="093333333";
		editemail ="selenium06"+ randomdata()+"@gmail.com";		
	}
	
	
	@Test
	public void TC_01_CreateNewCustomer() {
		// Function for testcase	
		
		// Login page			
		driver.get("http://demo.guru99.com/v4/");	
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();				
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys("mngr235123");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("qahEqEz");
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
		
		
		//Verify page Login
		Assert.assertTrue(driver.findElement(By.xpath("//marquee[text()=\"Welcome To Manager's Page of Guru99 Bank\"]")).isDisplayed());
		
		// Create New Customer
		driver.findElement(By.xpath("//a[text()='New Customer']")).click();			
		driver.findElement(By.xpath("//input[@name='name']")).sendKeys(customerName);
		driver.findElement(By.xpath("//input[@value='m']")).click();	
		driver.findElement(By.xpath("//input[@id='dob']")).sendKeys(birthOfDay);
		driver.findElement(By.xpath("//textarea [@name='addr']")).sendKeys(address);
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(city);
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys(state);
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys(pin);
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys(mobie);
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
		driver.findElement(By.xpath("//input[@name='sub']")).click();
		
		Assert.assertTrue(driver.findElement(By.xpath("//p[text()='Customer Registered Successfully!!!']")).isDisplayed());
		
		//Verify Customer created success
		customerID=driver.findElement(By.xpath("//td[text()='Customer ID']//following-sibling::td")).getText();
				
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Customer ID']//following-sibling::td")).getText(), customerID);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Customer Name']//following-sibling::td")).getText(), customerName);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Gender']//following-sibling::td")).getText(), "male");
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Birthdate']//following-sibling::td")).getText(), birthOfDay);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Address']//following-sibling::td")).getText(), address);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='City']//following-sibling::td")).getText(), city);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='State']//following-sibling::td")).getText(), state);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Pin']//following-sibling::td")).getText(), pin);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Mobile No.']//following-sibling::td")).getText(), mobie);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Email']//following-sibling::td")).getText(), email);		
		
	}	
	
	@Test
	public void TC_02_EditCustomer() {
		// Function for testcase			
		//Click Edit Customer Page
		
		driver.findElement(By.xpath("//a[text()='Edit Customer']")).click();
		driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys(customerID);		
		driver.findElement(By.xpath("//input[@name='AccSubmit']")).click();
		
		Assert.assertTrue(driver.findElement(By.xpath("//p[text()='Edit Customer']")).isDisplayed());		
		
		//Verify Information Name and Address of Customer
		WebElement nametexbox = driver.findElement(By.xpath("//input[@name='name']"));
		WebElement AddTextArea = driver.findElement(By.xpath("//textarea [@name='addr']"));		
		Assert.assertEquals(customerName, nametexbox.getAttribute("value"));
		Assert.assertEquals(address, AddTextArea.getText());
		
		//Edit Information of Customer	
	
		driver.findElement(By.xpath("//textarea [@name='addr']")).clear();
		driver.findElement(By.xpath("//input[@name='city']")).clear();
		driver.findElement(By.xpath("//input[@name='state']")).clear();
		driver.findElement(By.xpath("//input[@name='pinno']")).clear();
		driver.findElement(By.xpath("//input[@name='telephoneno']")).clear();
		driver.findElement(By.xpath("//input[@name='emailid']")).clear();
		
		driver.findElement(By.xpath("//textarea [@name='addr']")).sendKeys(editaddress);
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(editcity);
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys(editstate);
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys(editpin);
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys(editmobie);
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys(editemail);	
		driver.findElement(By.xpath("//input[@name='sub']")).click();
		
		
		// Verify edit Customer success
		Assert.assertTrue(driver.findElement(By.xpath("//p[text()='Customer details updated Successfully!!!']")).isDisplayed());
				
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Address']//following-sibling::td")).getText(), editaddress);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='City']//following-sibling::td")).getText(), editcity);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='State']//following-sibling::td")).getText(), editstate);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Pin']//following-sibling::td")).getText(), editpin);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Mobile No.']//following-sibling::td")).getText(), editmobie);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Email']//following-sibling::td")).getText(), editemail);		
		
	}	
	
	
	
	@AfterClass
	public void afterClass() {
		//Clear data
		driver.quit();
	}	
	public int randomdata() {
		Random random=new Random();
		int number = random.nextInt(999999);
		return number;
	}	
	
}
