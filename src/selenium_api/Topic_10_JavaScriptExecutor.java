package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_10_JavaScriptExecutor {
	WebDriver driver;	
	String email;
	String firstName, lastName, passWord;
	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		firstName = "A";
		lastName = "B";
		passWord ="123456";
	}

	@Test
	public void TC_01() {
		// Function for testcase
		driver.get("http:live.guru99.com/");
		String domainPage = (String) executeForBrowser("return document.domain");
		Assert.assertEquals(domainPage, "live.guru99.com");

		String urlPage = (String) executeForBrowser("return document.URL");
		Assert.assertEquals(urlPage, "http://live.guru99.com/");

		clickToElementByJS(driver.findElement(By.xpath("//a[text()='Mobile']")));
		String titlePageMobile = (String) executeForBrowser("return document.title;");
		Assert.assertEquals(titlePageMobile, "Mobile");

		WebElement addToCartSamsungGalaxy = driver.findElement(
				By.xpath("//h2[a[text()='Samsung Galaxy']]/following-sibling::div[@class='actions']//button"));
		clickToElementByJS(addToCartSamsungGalaxy);
		String textInPageShoppingCart = (String) executeForBrowser("return document.documentElement.innerText;");
		Assert.assertTrue(textInPageShoppingCart.contains("Samsung Galaxy was added to your shopping cart."));

		clickToElementByJS(driver.findElement(By.xpath("//a[text()='Privacy Policy']")));
		String titlePagePrivacyPolicy = (String) executeForBrowser("return document.title;");
		Assert.assertEquals(titlePagePrivacyPolicy, "Privacy Policy");

		scrollToBottomPage(driver);
		Assert.assertTrue(driver.findElement(By.xpath(
				"//th[text()='WISHLIST_CNT']/following-sibling::td[text()='The number of items in your Wishlist.']"))
				.isDisplayed());
	}

	@Test
	public void TC_02() {
		// Function for testcase
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
		WebElement iframeResult = driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		driver.switchTo().frame(iframeResult);
		WebElement lastName = driver.findElement(By.xpath("//input[@name='lname']"));
		removeAttributeInDOM(lastName, "disabled");
		sendkeyToElementByJS(lastName, "Automation Testing");
		WebElement btnSubmit = driver.findElement(By.xpath("//input[@value='Submit']"));
		clickToElementByJS(btnSubmit);
		// driver.switchTo().frame(iframeResult);
		Assert.assertTrue(
				driver.findElement(By.xpath("//div[contains(text(),'lname=Automation Testing')]")).isDisplayed());
	}

	@Test
	public void TC_03() throws Exception {
		// Function for testcase		
		email = "selenium06"+ randomdata()+"@gmail.com";		
		driver.get("http://live.guru99.com");
		
		// Click My Account -> Click Create Account
		clickToElementByJS(driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")));		
		clickToElementByJS(driver.findElement(By.xpath("//span[text()='Create an Account']")));
		
		// Input Data
		sendkeyToElementByJS(driver.findElement(By.xpath("//input[@id='firstname']")),firstName);
		sendkeyToElementByJS(driver.findElement(By.xpath("//input[@id='lastname']")),lastName);
		sendkeyToElementByJS(driver.findElement(By.xpath("//input[@id='email_address']")),email);
		sendkeyToElementByJS(driver.findElement(By.xpath("//input[@id='password']")),passWord);
		sendkeyToElementByJS(driver.findElement(By.xpath("//input[@id='confirmation']")),passWord);
		
		// Click Register
		clickToElementByJS(driver.findElement(By.xpath("//span[text()='Register']")));
		
		// Verify page 
		
		String verifyMessage= (String)executeForBrowser("return document.documentElement.innerText;");		
		Assert.assertTrue(verifyMessage.contains("Thank you for registering with Main Website Store.")); 
		
		// Logout
		clickToElementByJS(driver.findElement(By.xpath("//div[@class='page-header-container']//span[text()='Account']")));		
		clickToElementByJS(driver.findElement(By.xpath("//a[text()='Log Out']")));
		Thread.sleep(10000) ;		
		
		// Verify HomePage
		String homepageTitle = (String) executeForBrowser("return document.title;");		
		Assert.assertEquals(homepageTitle, "Home page");	
			
	}

	@AfterClass
	public void afterClass() {
		// Clear data
		driver.quit();
	}

	public void highlightElement(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.border='6px groove red'", element);
	}

	public Object executeForBrowser(String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object clickToElementByJS(WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object sendkeyToElementByJS(WebElement element, String value) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].setAttribute('value', '" + value + "')", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object removeAttributeInDOM(WebElement element, String attribute) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object navigateToUrlByJS(String url) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.location = '" + url + "'");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public int randomdata() {
		Random random = new Random();
		int number = random.nextInt(999999);
		return number;
	}
}
