package selenium_api;

import org.testng.annotations.Test;
import org.yaml.snakeyaml.tokens.DirectiveToken;

import bsh.commands.dir;

import org.testng.annotations.BeforeClass;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.server.browserlaunchers.DrivenSeleniumLauncher;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_11_UploadFile {
	WebDriver driver;
	String projectFolder = System.getProperty("user.dir");
	String fileName01 = "01.png";
	String fileName02 = "02.png";
	String fileName03 = "03.png";

	String filePath01 = projectFolder + "\\Upload\\" + fileName01;
	String filePath02 = projectFolder + "\\Upload\\" + fileName02;
	String filePath03 = projectFolder + "\\Upload\\" + fileName03;

	String folder = "duy123456";
	String email = "duy@gmail.com";
	String firstName = "quangduy";

	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		System.setProperty("webdriver.chrome.driver", ".\\lib\\chromedriver.exe");
		// System.setProperty("webdriver.ie.driver", ".\\lib\\IEDriverServer.exe");
		// driver = new FirefoxDriver();
		driver = new ChromeDriver();
		// driver = new InternetExplorerDriver();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	//@Test
	public void TC_01_Sendkey() throws Exception {
		// Function for testcase
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		Thread.sleep(5000);
		WebElement addFile = driver.findElement(By.xpath("//input[@type='file']"));
		// System.out.println(filePath01 + "\n " + filePath02 + "\n " + filePath03);
		addFile.sendKeys(filePath01 + "\n" + filePath02 + "\n" + filePath03);

		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name' and text()= '" + fileName01 + "']")).isDisplayed());
		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name' and text()= '" + fileName02 + "']")).isDisplayed());
		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name' and text()= '" + fileName03 + "']")).isDisplayed());

		List<WebElement> startButton = driver.findElements(By.xpath("//td//button[@class='btn btn-primary start']"));

		for (WebElement e : startButton) {
			e.click();
			Thread.sleep(3000);
		}
		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name']/a[@title='" + fileName01 + "']")).isDisplayed());
		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name']/a[@title='" + fileName02 + "']")).isDisplayed());
		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name']/a[@title='" + fileName03 + "']")).isDisplayed());

		List<WebElement> listImage = driver.findElements(By.xpath("//tr[@class='template-download fade in']//img"));
		for (WebElement e : listImage) {
			Assert.assertTrue(checkImageLoad(e));
		}
	}

	@Test
	public void TC_03_Robot() throws Exception {
		// Function for testcase
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		String browserName = driver.toString().toLowerCase();
		StringSelection select = new StringSelection(filePath01);

		// Step 1: Click Add file
		if (browserName.contains("chrome")) {
			driver.findElement(By.cssSelector(".fileinput-button")).click();

		} else if (browserName.contains("firefox")) {
			clickToElementByJS(driver.findElement(By.xpath("//input[@type='file']")));
		} else if (browserName.contains("InternetExplorer")) {
			clickToElementByJS(driver.findElement(By.xpath("//span[contains(text(),'Add files...')]")));
			Runtime.getRuntime().exec(new String[] { ".\\upload\\ie.exe", filePath01 });
		}

		// Step 2: Coppy FilePath01
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);

		// Step 3: Paste FilePath01

		Robot robot = new Robot();
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		// Step 4: Verify fileName01
		Thread.sleep(3000);
		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name' and text()= '" + fileName01 + "']")).isDisplayed());

		// Step 5: Click button Start
		clickToElementByJS(driver.findElement(By.xpath("//td//button[@class='btn btn-primary start']")));

		// Step 6: Verify fileName01 uploaded
		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name']/a[@title='" + fileName01 + "']")).isDisplayed());
		WebElement listImage = driver.findElement(By.xpath("//tr[@class='template-download fade in']//img"));

		checkImageLoad(listImage);

	}

	@Test
	public void TC_03_AutoIT() throws Exception {
		// Function for testcase
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		Thread.sleep(5000);
		String browserName = driver.toString().toLowerCase();

		if (browserName.contains("chrome")) {
			driver.findElement(By.cssSelector(".fileinput-button")).click();
			Runtime.getRuntime().exec(new String[] { ".\\upload\\chrome.exe", filePath01 });

		} else if (browserName.contains("firefox")) {
			clickToElementByJS(driver.findElement(By.xpath("//input[@type='file']")));
			Runtime.getRuntime().exec(new String[] { ".\\upload\\firefox.exe", filePath01 });
		} else if (browserName.contains("InternetExplorer")) {
			clickToElementByJS(driver.findElement(By.xpath("//span[contains(text(),'Add files...')]")));
			Runtime.getRuntime().exec(new String[] { ".\\upload\\ie.exe", filePath01 });
		}

		Thread.sleep(3000);
		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name' and text()= '" + fileName01 + "']")).isDisplayed());
		clickToElementByJS(driver.findElement(By.xpath("//td//button[@class='btn btn-primary start']")));
		Assert.assertTrue(
				driver.findElement(By.xpath("//p[@class='name']/a[@title='" + fileName01 + "']")).isDisplayed());

		WebElement listImage = driver.findElement(By.xpath("//tr[@class='template-download fade in']//img"));

		checkImageLoad(listImage);
	}

	//@Test
	public void TC_04() throws Exception {
		// Function for testcase
		driver.get("https://encodable.com/uploaddemo/");
		WebElement upLoadFile = driver.findElement(By.xpath("//input[@id='uploadname1']"));
		// System.out.println(filePath01 + "\n " + filePath02 + "\n " + filePath03);
		upLoadFile.sendKeys(filePath01);

		Select dropdownlistUploadTo = new Select(
				driver.findElement(By.xpath("//select[@class='upform_field picksubdir_field']")));
		dropdownlistUploadTo.selectByValue("/");

		driver.findElement(By.xpath("//input[@id='newsubdir1']")).sendKeys(folder);
		driver.findElement(By.xpath("//input[@id='formfield-email_address']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@id='formfield-first_name']")).sendKeys(firstName);
		driver.findElement(By.xpath("//input[@id='uploadbutton']")).click();
		Thread.sleep(5000);

		Assert.assertTrue(driver.findElement(By.xpath(
				"//dt[text()='Your information:']/following-sibling::dd[text()='First Name: " + firstName + "']"))
				.isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(
				"// dt[text()='Your information:']/following-sibling::dd[text()='Email Address: " + email + "']"))
				.isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath("//a[text()='" + fileName01 + "']")).isDisplayed());

		driver.findElement(By.xpath("//a[text()='View Uploaded Files']")).click();
		driver.findElement(By.xpath("//td[@class='dname diricon']//a[text()='" + folder + "']")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//a[text()='" + fileName01 + "']")).isDisplayed());
		WebElement image = driver.findElement(By.xpath("//img[contains(@src,'" + fileName01 + "')]"));
		Assert.assertTrue(checkImageLoad(image));
	}

	@AfterClass
	public void afterClass() {
		// Clear data
		driver.quit();
	}

	public boolean checkImageLoad(WebElement image) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		return (boolean) jsExecutor.executeScript("return arguments[0].complete && "
				+ "typeof arguments[0].naturalWidth != \"undefined\" && " + "arguments[0].naturalWidth > 0", image);
	}

	public Object clickToElementByJS(WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
}
