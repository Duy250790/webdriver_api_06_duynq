package selenium_api;

import org.testng.annotations.Test;

import com.google.common.base.Function;
import com.thoughtworks.selenium.Wait;

import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_12_AssertAndWait {
	WebDriver driver;
	By SelectedDate = By.xpath("//span[@id='ctl00_ContentPlaceholder1_Label1']");
	WebDriverWait  waitExplicit;
	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		driver = new FirefoxDriver();	
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		waitExplicit = new WebDriverWait(driver, 30);
	}
	
	
	//@Test
	public void TC_01_ImplicitWait() {
		// Function for testcase
		driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		driver.findElement(By.xpath("//div[@id='start']/button")).click();
		Assert.assertTrue(driver.findElement(By.xpath(".//div[@id='finish']/h4[text()='Hello World!']")).isDisplayed());	
	}	
	//@Test
	public void TC_02_ExplicitWait() {
		// Function for testcase
		driver.get("http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		
		// Check Date Time Picker Displayed
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='ctl00_ContentPlaceholder1_Panel1']")).isDisplayed());
		
		// Check selected Date = 'No Selected Dates to display.'
		WebElement afferSelectedDate = 	driver.findElement(SelectedDate);
		Assert.assertEquals(afferSelectedDate.getText(), "No Selected Dates to display.");
		
		// Click today 
		driver.findElement(By.xpath("//a[text()='6']")).click();
		By ajaxIcon = By.xpath("//div[@class='raDiv']");
		waitExplicit.until(ExpectedConditions.invisibilityOfElementLocated(ajaxIcon));
		
		//Verfiy today selected		
		Assert.assertTrue(driver.findElement(By.xpath("//td[@class='rcSelected']//a[text()='6']")).isDisplayed());
		
		
		
		WebElement beforSelectedDate = 	driver.findElement(SelectedDate);
		
		Assert.assertEquals(beforSelectedDate.getText(), "Tuesday, November 06, 2018");
	}	
	@Test
	public void TC_03_FluentWait() {
		// TODO Auto-generated method stub
		driver.get("https://daominhdam.github.io/fluent-wait/");
		WebElement countdount =  driver.findElement(By.xpath("//div[@id='javascript_countdown_time']"));
		waitExplicit.until(ExpectedConditions.visibilityOf(countdount));

		// Khởi tạo Fluent wait
		new FluentWait<WebElement>(countdount)
		           // Tổng time wait là 15s
		           .withTimeout(15, TimeUnit.SECONDS)
		            // Tần số mỗi 1s check 1 lần
		            .pollingEvery(1, TimeUnit.SECONDS)
		           // Nếu gặp exception là find ko thấy element sẽ bỏ  qua
		            .ignoring(NoSuchElementException.class)
		            // Kiểm tra điều kiện
		            .until(new Function<WebElement, Boolean>() {
		                public Boolean apply(WebElement element) {
		                           // Kiểm tra điều kiện countdount = 00
		                           boolean flag =  element.getText().endsWith("00");
		                           System.out.println("Time = " +  element.getText());
		                           // return giá trị cho function apply
		                           return flag;
		                      }
		               });
	}
	@AfterClass
	public void afterClass() {
		//Clear data
		driver.quit();
	}

}
