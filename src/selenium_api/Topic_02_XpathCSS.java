package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_02_XpathCSS {
	WebDriver driver;
	String email;
	int random;

	
	
	
	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		driver = new FirefoxDriver();			
	}
	
	
	@Test
	public void TC_01_VerifyURLandtitle() {
		// Function for testcase	
		driver.get("http://live.guru99.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();		
		String homepageTitle = driver.getTitle();		
		Assert.assertEquals(homepageTitle, "Home page");		
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();		
		driver.findElement(By.xpath("//span[text()='Create an Account']")).click();		
		driver.navigate().back();		
		String pageloginUrl = driver.getCurrentUrl();		
		Assert.assertEquals(pageloginUrl, "http://live.guru99.com/index.php/customer/account/login/");		
		driver.navigate().forward();		
		String createaccountUrl=driver.getCurrentUrl();		
		Assert.assertEquals(createaccountUrl, "http://live.guru99.com/index.php/customer/account/create/");
	}	
	
	@Test
	public void TC_02_Loginempty() {
		// Function for testcase	
		driver.get("http://live.guru99.com");			
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		
		String errorMessageEmail=driver.findElement(By.xpath("//div[@id='advice-required-entry-email']")).getText(); 
		Assert.assertEquals(errorMessageEmail, "This is a required field.");
		
		String errorMessagePass=driver.findElement(By.xpath("//div[@id='advice-required-entry-pass']")).getText(); 
		Assert.assertEquals(errorMessagePass, "This is a required field.");	
	}	
	
	@Test
	public void TC_03_LoginwithEmailinvalid() {
		// Function for testcase	
		driver.get("http://live.guru99.com");			
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
		
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("23434234@12312.123123");
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		String errorMessageEmail=driver.findElement(By.xpath("//div[@id='advice-validate-email-email']")).getText(); 
		Assert.assertEquals(errorMessageEmail, "Please enter a valid email address. For example johndoe@domain.com.");
	}	
	
	@Test
	public void TC_04_LoginwithPasswordincorrect() {
		// Function for testcase	
		driver.get("http://live.guru99.com");			
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();		
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("automation@gmail.com");
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("123");
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		String errorMessageEmail=driver.findElement(By.xpath("//div[@id='advice-validate-password-pass']")).getText(); 
		Assert.assertEquals(errorMessageEmail, "Please enter 6 or more characters without leading or trailing spaces.");		
	
	}	
	
	@Test 
	public void TC_05_CreateAccount() throws InterruptedException {
		// Function for testcase	
		email = "selenium06"+ randomdata()+"@gmail.com";		
		driver.get("http://live.guru99.com");
		driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();		
		driver.findElement(By.xpath("//span[text()='Create an Account']")).click();
		driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("A");
		driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("B");
		driver.findElement(By.xpath("//input[@id='email_address']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//input[@id='confirmation']")).sendKeys("123456");
		driver.findElement(By.xpath("//span[text()='Register']")).click();
		String verifyMessage=driver.findElement(By.xpath("//span[text()='Thank you for registering with Main Website Store.']")).getText();
		Assert.assertEquals(verifyMessage, "Thank you for registering with Main Website Store.");
		driver.findElement(By.xpath("//div[@class='page-header-container']//span[text()='Account']")).click();
		driver.findElement(By.xpath("//a[text()='Log Out']")).click();
		Thread.sleep(10000) ;
		//WebElement hompagetitle= driver.findElement(By.xpath("//title[text()='Home page']"));
		
		//Assert.assertTrue(hompagetitle.isDisplayed());		
		
		String homepageTitle = driver.getTitle();
		
		Assert.assertEquals(homepageTitle, "Home page");	
	}		
	
	@AfterClass
	public void afterClass() {
		//Clear data
		driver.quit();
	}
	
	public int randomdata() {
		Random random=new Random();
		int number = random.nextInt(999999);
		return number;
	}	

}
