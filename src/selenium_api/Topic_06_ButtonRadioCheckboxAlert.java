package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_06_ButtonRadioCheckboxAlert {
	WebDriver driver;
	String user ="admin";
	String password="admin";
	
	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		driver = new FirefoxDriver();		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	
	@Test
	public void TC_01_Button() {
		// Function for testcase
		driver.get("http:live.guru99.com/");
		WebElement myAccountLink =driver.findElement(By.xpath("//div[@class='footer-container']//a[text()='My Account']"));
		clickElementByJavaScript(myAccountLink);
		WebElement loginForm =driver.findElement(By.xpath("//form[@id='login-form']"));
		Assert.assertTrue(isControlDisplay(loginForm));
		
		
		WebElement createAnAccountButton =driver.findElement(By.xpath("//span[text()='Create an Account']"));
		clickElementByJavaScript(createAnAccountButton);
		WebElement createAccountForm=driver.findElement(By.xpath("//div[@class='account-create']"));
		Assert.assertTrue(isControlDisplay(createAccountForm));
	}	
	
	@Test
	public void TC_02_CheckBox() throws InterruptedException {
		// Function for testcase
		driver.get("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		WebElement dualZoneAirConditioningRaido =driver.findElement(By.xpath("//label[text()='Dual-zone air conditioning']//preceding-sibling::input"));
		clickElementByJavaScript(dualZoneAirConditioningRaido);
		Thread.sleep(1000);
		Assert.assertTrue(dualZoneAirConditioningRaido.isSelected());
		
	}	
	
	@Test
	public void TC_03_RadioButton() throws InterruptedException {
		// Function for testcase
		driver.get("http://demos.telerik.com/kendo-ui/styling/radios");
		WebElement PetrolRaido =driver.findElement(By.xpath("//label[text()='1.8 Petrol, 118kW']//preceding-sibling::input"));
		clickElementByJavaScript(PetrolRaido);
		Thread.sleep(1000);
		Assert.assertTrue(PetrolRaido.isSelected());		
	}	
	
	@Test
	public void TC_04_JSAlert() throws InterruptedException {
		// Function for testcase
		driver.get("http://daominhdam.890m.com/");
		WebElement jsAlertButton =driver.findElement(By.xpath("//button[text()='Click for JS Alert']"));
		jsAlertButton.click();
		Alert jsAlert = driver.switchTo().alert();
		Assert.assertEquals(jsAlert.getText(), "I am a JS Alert");
		jsAlert.accept();
		WebElement message = driver.findElement(By.xpath("//p[text()='You clicked an alert successfully ']"));	
		Assert.assertTrue(isControlDisplay(message));
	}
	
	@Test
	public void TC_05_JSConfirm() throws InterruptedException {
		// Function for testcase
		driver.get("http://daominhdam.890m.com/");
		WebElement jsAlertConfirmButton =driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
		jsAlertConfirmButton.click();
		Alert jsAlert = driver.switchTo().alert();
		Assert.assertEquals(jsAlert.getText(), "I am a JS Confirm");
		jsAlert.dismiss();
		WebElement message = driver.findElement(By.xpath("//p[text()='You clicked: Cancel']"));	
		Assert.assertTrue(isControlDisplay(message));
	}
	
	@Test
	public void TC_06_JSPrompt() throws InterruptedException {
		// Function for testcase
		driver.get("http://daominhdam.890m.com/");
		WebElement jsPromptButton =driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
		jsPromptButton.click();
		Alert jsAlert = driver.switchTo().alert();
		Assert.assertEquals(jsAlert.getText(), "I am a JS prompt");
		jsAlert.sendKeys("selenium06online");
		jsAlert.accept();
		WebElement message = driver.findElement(By.xpath("//p[text()='You entered: selenium06online']"));	
		Assert.assertTrue(isControlDisplay(message));
	}	
	
	@Test
	public void TC_07_handleAuthentication() throws InterruptedException {
		// Function for testcase
		String baseUrl ="http://" + user + ":" + password + "@" + "the-internet.herokuapp.com/basic_auth/";		
		driver.get(baseUrl);		
		WebElement message = driver.findElement(By.xpath("//p[contains(text(),'Congratulations! You must have the proper credentials.')]"));
		Assert.assertTrue(isControlDisplay(message));	
	
	}	


	@AfterClass
	public void afterClass() {
		//Clear data
		driver.quit();
	}
	
	public void clickElementByJavaScript(WebElement element) {
		// TODO Auto-generated method stub
		JavascriptExecutor je =((JavascriptExecutor) driver);
		je.executeScript("arguments[0].click();", element);
	} 
	
	public boolean isControlDisplay(WebElement element)
	{
		return element.isDisplayed();
	}

}
