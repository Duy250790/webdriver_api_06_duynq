package selenium_api;

import org.testng.annotations.Test;

import com.sun.org.apache.xerces.internal.impl.xpath.XPath;

import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_03_WebBrowser_WebElement {
	WebDriver driver;
	WebElement element;	
	
	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		driver = new FirefoxDriver();			
	}
	
	
	@Test
	public void TC_01_checkElementDisplayInPage() {
		// Function for testcase	
		driver.get("https://daominhdam.github.io/basic-form/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		WebElement email=driver.findElement(By.xpath("//input[@id='mail']"));
		if(email.isDisplayed()){
			email.sendKeys("Automation Testing");
		}
		
		WebElement Age=driver.findElement(By.xpath("//input[@id='under_18']"));		
		if(Age.isDisplayed()) {
			Age.click();
		}
		
		WebElement Education=driver.findElement(By.xpath("//textarea[@id='edu']"));
		if(Education.isDisplayed()) {
			Education.sendKeys("Automation Testing");
		}
		
	}
	
	@Test
	public void TC_02_checkElementEnableInPage() {
		// Function for testcase	
		driver.get("https://daominhdam.github.io/basic-form/");	
		
		WebElement emailTextbox=driver.findElement(By.xpath("//input[@id='mail']"));		
		WebElement Under18Readio=driver.findElement(By.xpath("//input[@id='under_18']"));			
		WebElement Educationtextarea=driver.findElement(By.xpath("//textarea[@id='edu']"));
		WebElement JobRole01Combobox=driver.findElement(By.xpath("//select[@id='job1']"));
		WebElement JobInterests_DevelopmentCheckbox=driver.findElement(By.xpath("//input[@id='development']"));
		WebElement Slider01=driver.findElement(By.xpath("//input[@id='slider-1']"));
		
		WebElement passwordTextbox=driver.findElement(By.xpath("//input[@id='password']"));		
		WebElement RadioButtonisDisable=driver.findElement(By.xpath("//input[@id='radio-disabled']"));			
		WebElement Biographytextarea=driver.findElement(By.xpath("//textarea[@id='bio']"));
		WebElement JobRole02Combobox=driver.findElement(By.xpath("//select[@id='job2']"));
		WebElement CheckboxisDisable=driver.findElement(By.xpath("//input[@id='check-disbaled']"));
		WebElement Slider02=driver.findElement(By.xpath("//input[@id='slider-2']"));
		
		
		IsElementEnable(emailTextbox);
		IsElementEnable(Under18Readio);
		IsElementEnable(Educationtextarea);
		IsElementEnable(JobRole01Combobox);
		IsElementEnable(JobInterests_DevelopmentCheckbox);
		IsElementEnable(Slider01);
		
		IsElementEnable(passwordTextbox);
		IsElementEnable(RadioButtonisDisable);
		IsElementEnable(Biographytextarea);
		IsElementEnable(JobRole02Combobox);
		IsElementEnable(CheckboxisDisable);
		IsElementEnable(Slider02);
	}	
	
	@Test
	public void TC_03_checkElementcheckedInPage() {
		// Function for testcase	
		driver.get("https://daominhdam.github.io/basic-form/");			
				
		WebElement Under18Radio=driver.findElement(By.xpath("//input[@id='under_18']"));	
		Under18Radio.click();
		WebElement JobInterests_DevelopmentCheckbox=driver.findElement(By.xpath("//input[@id='development']"));	
		JobInterests_DevelopmentCheckbox.click();
		
		if(Under18Radio.isSelected() ==false)
		{
			Under18Radio.click();
		}
		
		if(JobInterests_DevelopmentCheckbox.isSelected() ==false)
		{
			JobInterests_DevelopmentCheckbox.click();
		}
		
	}	
	@AfterClass
	public void afterClass() {
		//Clear data
		//driver.quit();
	}
	
	public void IsElementEnable(WebElement element) {
		if(element.isEnabled()){
			System.out.println( "Element " + element.getAttribute("name") + " is enabled");
		}
		else System.out.println("Element" + element.getAttribute("name") + " is disabled");
	}
}
