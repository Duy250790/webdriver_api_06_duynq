package selenium_api;

import static org.testng.AssertJUnit.assertArrayEquals;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import com.sun.org.apache.xerces.internal.impl.xpath.XPath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class Topic_05_Handel_DropDownList {
	WebDriver driver;
	WebElement element;
	WebDriverWait wait;
	String expectedValue = "19";
	String inputvalue = "A";
	int itemsSelected = 0;
	int expectedselectvalue = 4;
	List<String> listItemsSelected;
	int numberSelect;

	@BeforeClass
	public void beforeClass() {
		// khởi tao data - pre condition
		// driver = new FirefoxDriver();

		System.setProperty("webdriver.chrome.driver", ".\\lib\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 10);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();

	}

	//@Test
	public void TC_01_HandleHTMLDropDownList() {
		// Function for testcase
		driver.get("https://daominhdam.github.io/basic-form/");

		Select JobRole01DropDown = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		AssertJUnit.assertFalse(JobRole01DropDown.isMultiple());

		JobRole01DropDown.selectByVisibleText("Automation Tester");
		AssertJUnit.assertEquals("Automation Tester", JobRole01DropDown.getFirstSelectedOption().getText());

		JobRole01DropDown.selectByValue("manual");
		AssertJUnit.assertEquals("manual", JobRole01DropDown.getFirstSelectedOption().getAttribute("value"));

		JobRole01DropDown.selectByIndex(3);
		AssertJUnit.assertEquals("Mobile Tester", JobRole01DropDown.getFirstSelectedOption().getText());

		AssertJUnit.assertEquals(5, JobRole01DropDown.getOptions().size());

	}

	//@Test
	public void TC_02_HandleCustomDropDownList() throws Exception {
		// Function for testcase
		// Jquery
		driver.get("http://jqueryui.com/resources/demos/selectmenu/default.html");
		selectItemScrollDropDown("//Span[@id='number-button']",
				"//ul[@id='number-menu']//li[@class='ui-menu-item']//div[@role='option']", expectedValue);
		AssertJUnit.assertEquals(expectedValue, driver.findElement(By.xpath("//Span[@id='number-button']")).getText());

		// Angular
		driver.get("https://material.angular.io/components/select/examples");
		selectItemScrollDropDown("//mat-select[@placeholder='State']", "//mat-option//span", "New Jersey");
		AssertJUnit.assertEquals(
				driver.findElement(By.xpath("//mat-select[@placeholder='State']//span//span")).getText(), "New Jersey");
		Thread.sleep(1500);
		selectItemScrollDropDown("//mat-select[@placeholder='State']", "//mat-option//span", "Wyoming");
		AssertJUnit.assertEquals(
				driver.findElement(By.xpath("//mat-select[@placeholder='State']//span//span")).getText(), "Wyoming");

		// telerik1
		driver.get("https://demos.telerik.com/kendo-ui/dropdownlist/index");
		selectItemScrollDropDown("//span[@aria-owns='color_listbox']//span[@class='k-input']",
				"//div[@id='color-list']//li[@role='option']", "Grey");
		AssertJUnit.assertEquals(
				driver.findElement(By.xpath("//span[@aria-owns='color_listbox']//span[@class='k-input']")).getText(),
				"Grey");
		Thread.sleep(1500);
		selectItemScrollDropDown("//span[@aria-owns='color_listbox']//span[@class='k-input']",
				"//div[@id='color-list']//li[@role='option']", "Orange");
		AssertJUnit.assertEquals(
				driver.findElement(By.xpath("//span[@aria-owns='color_listbox']//span[@class='k-input']")).getText(),
				"Orange");

		// telerik2
		driver.get("https://mikerodham.github.io/vue-dropdowns/");
		selectItemScrollDropDown("//li[@class='dropdown-toggle']", "//ul[@class='dropdown-menu']//li", "Second Option");
		AssertJUnit.assertEquals(driver.findElement(By.xpath("//li[@class='dropdown-toggle']")).getText(),
				"Second Option");
		Thread.sleep(1500);
		selectItemScrollDropDown("//li[@class='dropdown-toggle']", "//ul[@class='dropdown-menu']//li", "Third Option");
		AssertJUnit.assertEquals(driver.findElement(By.xpath("//li[@class='dropdown-toggle']")).getText(),
				"Third Option");
	}

	@Test
	public void TC_03_HandleCustomDropDownList_More() throws Exception {
		// Function for testcase

		// Editable
		driver.get("http://indrimuska.github.io/jquery-editable-select/");
		inputandSelectItemDropDown("//div[@id='default-place']/input",
				"//div[@id='default-place']//ul[@class='es-list']//li[@class='es-visible' or @class='es-visible selected']",
				inputvalue, "Lancia");
		AssertJUnit.assertEquals(
				driver.findElement(By.xpath("//div[@id='default-place']//ul//li[@class='es-visible']")).getText(),
				"Lancia");

		// Advanced
		driver.get("http://wenzhixin.net.cn/p/multiple-select/docs/");
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1000)");
		selectMultipleItemDropDown("//p[@id='e1_t']//button[@class='ms-choice']",
				"//p[@id='e1_t']//div[@class='ms-drop bottom']//input[@data-name='selectItem']");
		AssertJUnit.assertEquals(driver.findElement(By.xpath("//p[@id='e1_t']//button[@class='ms-choice']")).getText(),
				expectedValue);
		
		// Multiple Select1
		driver.get("https://semantic-ui.com/modules/dropdown.html");
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1000)");
		selectionMultipleItemDropDown("//div[@class='ui fluid dropdown selection multiple']",
				"//div[@class='menu transition visible']//div[@class='item']");
		AssertJUnit.assertEquals(valuesSelected("//div[@class='dropdown example']//a[@class='ui label transition visible']"), listItemsSelected);
		
		Thread.sleep(1500);
		
		// Multiple Select2		
		selectionMultipleItemDropDown("//div[@data-tab='definition']//div[@class='ui fluid multiple search selection dropdown']",
				"//div[@class='menu transition visible']//div[@class='item']");
		AssertJUnit.assertEquals(valuesSelected("//div[@class='another dropdown example']//a[@class='ui label transition visible']"), listItemsSelected);
	}

	public void selectionMultipleItemDropDown(String parenLocator, String allItemLocator) throws Exception {
		// Step 1: Scroll and click to open dropdown
		itemsSelected = 0;
		
		WebElement parentDropdown = driver.findElement(By.xpath(parenLocator));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", parentDropdown);

		Thread.sleep(1000);
		List<WebElement> allItemsDropdown = driver.findElements(By.xpath(allItemLocator));
		numberSelect=allItemsDropdown.size();
		// 2. Wait for all Items visible
		wait.until(ExpectedConditions.visibilityOfAllElements(allItemsDropdown));
		listItemsSelected = new ArrayList<>();
		for (WebElement e : allItemsDropdown) {
			
			Thread.sleep(100);
			e = randomdata(allItemsDropdown);
			listItemsSelected.add(e.getText());
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", e);
			e.click();
			
			Thread.sleep(1500);
			allItemsDropdown= driver.findElements(By.xpath(allItemLocator));			
			itemsSelected = itemsSelected + 1;
			if (itemsSelected == expectedselectvalue) {

				break;
			}
		}
		System.out.println(itemsSelected);
	}

	public void selectMultipleItemDropDown(String parenLocator, String allItemLocator) throws Exception {
		// Step 1: Scroll and click to open dropdown
		WebElement parentDropdown = driver.findElement(By.xpath(parenLocator));
		itemsSelected=0;
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", parentDropdown);

		Thread.sleep(1000);
		List<WebElement> allItemsDropdown = driver.findElements(By.xpath(allItemLocator));
		// 2. Wait for all Items visible
		wait.until(ExpectedConditions.visibilityOfAllElements(allItemsDropdown));
		listItemsSelected = new ArrayList<>();
		for (WebElement e : allItemsDropdown) {			
			e = randomdata(allItemsDropdown);
			if (!e.isSelected()) {
				//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", e);
				e.click();
				Thread.sleep(1000);
				itemsSelected = itemsSelected + 1;
			}
			if (itemsSelected == expectedselectvalue) {
				break;
			}
			// Thread.sleep(1000);
		}
		expectedValue = itemsSelected + " of " + allItemsDropdown.size() + " selected";		
	}

	public void inputandSelectItemDropDown(String parenLocator, String allItemLocator, String inputvalue,
			String expectedValue) throws Exception {
		// Step 1: Scroll and click to open dropdown
		WebElement parentDropdown = driver.findElement(By.xpath(parenLocator));		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", parentDropdown);
		// parentDropdown.click();
		Thread.sleep(1000);
		parentDropdown.sendKeys(inputvalue);

		List<WebElement> allItemsDropdown = driver.findElements(By.xpath(allItemLocator));
		numberSelect =allItemsDropdown.size();
		// 2. Wait for all Items visible
		wait.until(ExpectedConditions.visibilityOfAllElements(allItemsDropdown));

		for (int i=0;i<=numberSelect;i++)
		{
			
			if (allItemsDropdown.get(i).getText().equals(expectedValue)) {
				//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", allItemsDropdown.get(i));
				Thread.sleep(500);
				allItemsDropdown.get(i).click();
				break;
			}
		}
	}
	
	public void selectItemScrollDropDown(String parenLocator, String allItemLocator, String expectedValue)
			throws Exception {
		// Step 1: Scroll and click to open dropdown
		WebElement parentDropdown = driver.findElement(By.xpath(parenLocator));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", parentDropdown);
		// Thread.sleep(1000);
		List<WebElement> allItemsDropdown = driver.findElements(By.xpath(allItemLocator));
		// 2. Wait for all Items visible
		wait.until(ExpectedConditions.visibilityOfAllElements(allItemsDropdown));
		for (WebElement e : allItemsDropdown) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", e);
			Thread.sleep(100);
			if (e.getText().equals(expectedValue)) {
				e.click();
				break;
			}
		}

	}

	@AfterClass
	public void afterClass() {
		// Clear data
		driver.quit();
	}
	
	public List<String> valuesSelected(String listItemSelection)
	{
		List<WebElement> allItemsDropdown = driver.findElements(By.xpath(listItemSelection));
		List<String> itemsSelected = new ArrayList<>();
		for(WebElement e: allItemsDropdown)
		{
			itemsSelected.add(e.getText());
		}
		return itemsSelected;
	}
	

	public WebElement randomdata(List<WebElement> allItemsDropdown) {
		Random random = new Random();
		WebElement element = allItemsDropdown.get(random.nextInt(allItemsDropdown.size()));
		return element;
	}

}
