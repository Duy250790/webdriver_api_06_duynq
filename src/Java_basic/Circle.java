package Java_basic;

public class Circle {
	private float radius = 0f;
	private float cd = 0f;
	private float cr = 0f;
	public static final float PI=3.14159f;
	
	public Circle (int rad) {
		this.radius = rad;
	}
	
	public Circle (int a, int b) {
		this.cd = a;
		this.cr = b;
	}
	
	public float getChuvi() {
		return 2* PI*radius;
	}
	
	public float getChuvihcn() {
		return (cd+cr)*2;
	}
	
	public float getDientich() {
		return PI*radius*radius;
	}
	
	public float getDientichhcn() {
		return cd*cr;
	}
}
